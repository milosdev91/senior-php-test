<div class="container">
    <div class="alert alert-warning">
        <span>We are sorry. User with this email has already been registered.</span>
    </div>
    <div class="pull-right">
        <a id="back" href="/user/register">Back to register page</a>
    </div>
</div>