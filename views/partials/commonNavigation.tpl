<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="/home/index">Home page</a>
        <div class="navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <?php if(!User::isLogged()) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="/user/login">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/user/register">Register</a>
                </li>
                <?php endif ?>
                <?php if(User::isLogged()) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="/user/logout">Logout</a>
                </li>
                <?php endif ?>
                <li class="nav-item">
                    <form class="form-inline" method="POST" action="/search/submit">
                        <div class="form-group">
                            <input type="search" name="search" class="form-control" id="search" placeholder="Search">
                        </div>
                        <div class="form-group">
                            <input type="submit" value='Search' class="btn btn-primary"></input>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="delimiter"></div>