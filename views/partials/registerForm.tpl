<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <form method="POST" action="/user/postRegister">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" name="email" class="form-control" id="email" required>
                </div>
                <div class="form-group">
                    <label for="username">Name:</label>
                    <input type="username" name="name" class="form-control" id="pwd" required>
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" name="password" class="form-control" id="pwd" required>
                </div>
                <div class="form-group">
                    <label for="repeat_password">Repeat password:</label>
                    <input type="password" name="repeat_password" class="form-control" id="repeat_password" required>
                </div>
                <div class="clearfix">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>