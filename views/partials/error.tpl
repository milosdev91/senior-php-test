<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger">
                <span><?=$_SESSION['message']?></span></br>
                <span>Please, feel free to contact our administrators at </span>
                <strong><a href="mailto:<?php echo(DEV_EMAIL);?>"><?php echo(DEV_EMAIL);?></a></strong>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="javascript:history.back()">Back</a>
        </div>
    </div>
</div>
<?php $_SESSION['message'] = ''; ?>