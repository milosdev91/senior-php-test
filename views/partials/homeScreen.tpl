<div class="container">
    <?php if(isset($_SESSION['message']) && $_SESSION['message']) : ?>
    <div class="alert alert-info">
        <?=$_SESSION['message']?>
    </div>
    <?php endif ?>
    <div class="jumbotron">
        <h2>
            This is the Home page.
        </h2>
    </div>
</div>
<?php $_SESSION['message'] = '';?>