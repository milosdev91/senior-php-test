<div class="container">
    <?php if(isset($_SESSION['message']) && $_SESSION['message']) : ?>
        <div class="alert alert-warning">
            <span><strong><?=$_SESSION['message']?></strong></span>
        </div>
    <?php endif ?>
    <form class="form-signin" method="POST" action="/user/postLogin">
        <h2 class="form-signin-heading">Log in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>
<?php $_SESSION['message'] = '';?>
