<?php
    include(ROOT_DIR . 'views/partials/commonHeader.tpl');
    include(ROOT_DIR . 'views/partials/commonNavigation.tpl');
    $templateName = isset($templateName) ? $templateName : '';
    if(file_exists(ROOT_DIR . 'views/' . $templateName. '.tpl'))
    {
        include(ROOT_DIR . 'views/' . $templateName . '.tpl');
    }
    else
    {
        include(ROOT_DIR . 'views/partials/404.tpl');
    }
    include(ROOT_DIR . 'views/partials/footer.tpl');
?>
