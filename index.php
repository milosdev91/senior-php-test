<?php

if(PHP_SESSION_NONE == session_status())
{
    session_start();
}

define('ROOT_DIR', dirname(__FILE__) . '/');
define('INCLUDE_DIR', ROOT_DIR . 'includes/');

require_once(INCLUDE_DIR . 'config.php');
require_once(CLASSES_DIR . 'router.php');
require_once(CLASSES_DIR . 'connection.php');
require_once(CLASSES_DIR . 'user.php');
require_once(CLASSES_DIR . 'home.php');
require_once(CLASSES_DIR . 'search.php');


$router = new Router;
$start = $router->runRouter();

$view = new View();

// If router returns false, show error view to user
if(!$start)
{
    echo $view->addData(array('templateName' => 'partials/404'))->fetch('layout.master.tpl');
    return;
}

extract($start);

// If router returns array with class and method, execute
if($class && class_exists($class))
{
    $class1 = new $class;
    if(isset($method) && method_exists($class1, $method))
    {
        $class1->{$method}();
        return;
    }
}

// If neither, show error view
echo $view->addData(array('templateName' => 'partials/404'))->fetch('layout.master.tpl');
return true;
