<?php

/**
 * Connection class
 */
class Connection
{

    public static $link;
    protected $host;
    protected $user;
    protected $pass;
    protected $query;
    protected $dbName;

    function __construct()
    {
        $this->host = DB_HOST;
        $this->user = DB_USER;
        $this->pass = DB_PASSWORD;
        $this->dbName = DB_NAME;
    }

    /**
     * Connect to Database
     *
     * @param void
     *
     * @return object - database link
     */
    public function connect()
    {
        self::$link = mysqli_connect($this->host, $this->user, $this->pass);

        if(!self::$link)
        {
            $this->_errorAction('Error, can\'t connect to the database server', $this->link);
            exit;
        }

        if(false === mysqli_select_db(self::$link, $this->dbName))
        {
            $this->_errorAction("Error. Can't connect to the database");
            exit;
        }

        // Unset data so it can't be dumped
        $this->host = '';
        $this->user = '';
        $this->pass = '';

        return self::$link;
    }

    /**
     * Send error(s) to developer via email
     *
     * @param string $message - message to be sent
     * @param object $rerourceLink - database link
     * 
     * @return - throw an exception
     */
    private function _errorAction($message = false)
    {
        $message = ($message) ? $message : mysqli_error(self::$link) . ' SQL: ' . $this->query;
        $message = "An error occured on your project.</br>" . $message;
        mail(DEV_EMAIL, DEFAULT_ERROR_MAIL_SUBJECT, $message);
        throw new Exception($message);
	}

    /**
     * Prepare and execute query
     *
     * @param string $query - query to be executed
     *
     * @return object $result
    */
    public function query($query)
    {
        $this->query = $query;
        try
        {
            $result = mysqli_query(self::$link, $this->query);
        }
        catch(Exception $e)
        {
            // use for debugging
        }

        if(!$result)
        {
            $this->_errorAction(false, self::$link);
        }
        return $result;
    }

}
