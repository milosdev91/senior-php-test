<?php

/**
 * Utilities class
 */
class Utilities
{

    /**
     * Redirect method
     * 
     * @param string $location - redirect location
     *
     * @return void
     */
    public static function redirect($location)
    {
        header('Location: ' . $location);
        exit();
    }

    /**
     * Print error messages from session
     * 
     * @param void
     *
     * @return void
     */
    public function printError()
    {
        $view = new View;
        echo $view->addData(array('templateName' => 'partials/error'))->fetch('layout.master.tpl');
        exit();
    }

}
