<?php

require_once('utilities.php');
require_once('connection.php');

/**
 * User class
 */
class User
{
    private $link;
    private $utilities;
    private $connection;

    public function __construct()
    {
        $this->utilities = new Utilities;
        $this->link = new Connection;
        $this->connection = $this->link->connect();
    }
    /**
     * Register method
     *
     * @param string $username - username
     * @param string $pass - password
     *
     * @return void | redirect
     */
    public function postRegister()
    {
        $name = mysqli_real_escape_string($this->connection, trim($_POST['name']));
        $email = mysqli_real_escape_string($this->connection, trim($_POST['email']));
        $pass = mysqli_real_escape_string($this->connection, trim($_POST['password']));
        $passVerify = mysqli_real_escape_string($this->connection, trim($_POST['repeat_password']));

        // Verify email address
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $_SESSION['message'] = "Email address is not in correct format.";
            $this->utilities->printError();
        }

        // Verify password
        if($pass !== $passVerify)
        {
            $_SESSION['message'] = "Passwords don't match";
            $this->utilities->printError();
        }

        // Check if user already exists with provided email
        $query = "SELECT email FROM users WHERE email='$email'";
        try
        {
            $row = $this->link->query($query);
        }
        catch(Exception $e)
        {
            // use for debugging
        }

        $result = mysqli_fetch_assoc($row);

        if($result)
        {
            $_SESSION['message'] = "User with this email address has already been registered.";
            $this->utilities->printError();
        }

        $query = "INSERT INTO users (name, email, password, active)
                    VALUES ('$name', '$email', '" . md5($pass) . "', 1)";

        try
        {
            $result = $this->link->query($query);
        }
        catch(Exception $e)
        {
            // use for debugging
        }

        if($result)
        {
            // Login with provided credentials & redirect to index page
            $this->postLogin();
        }
        else
        {
            $_SESSION['message'] = 'Sorry. An error has occured and the user could not be registered.';
            $this->utilities->printError();
        }
    }

    /**
     * Show login form
     */
    public function login()
    {
        $view = new View;
        echo $view->addData(array('pageTitle' => 'Zadatak Login','templateName' => 'partials/loginForm'))->fetch('layout.master.tpl');
        return true;
    }

    /**
     * Show register form
     */
    public function register()
    {
        $view = new View;
        echo $view->addData(array('pageTitle' => 'Zadatak Register','templateName' => 'partials/registerForm'))->fetch('layout.master.tpl');
        return true;
    }

    /**
     * Login method
     */
    public function postLogin()
    {
        $email = mysqli_real_escape_string($this->connection, trim($_POST['email']));
        $pass = mysqli_real_escape_string($this->connection, trim($_POST['password']));

        if(!$email || !$pass)
        {
            return false;
        }
        $query = "SELECT id, name, email FROM users WHERE email='$email' AND password='" . md5($pass) . "' AND active=1";

        try
        {
            $row = $this->link->query($query);
        }
        catch(Exception $e)
        {
            // use for debugging
        }

        $result = mysqli_fetch_assoc($row);

        if($result)
        {
            $_SESSION['logged'] = 1;
            $_SESSION['id'] = $result['id'];
            $_SESSION['email'] = $result['email'];
            $_SESSION['message'] = 'Welcome, ' . $result['name'] . '!';
        }
        else
        {
            $_SESSION['message'] = "Error logging you in.";
            $this->utilities->printError();
        }

        setcookie("email", $email);
        if($remember)
        {
            setcookie("remember", 1);
        }

        // Redirect to home screen if user was logged in
        Utilities::redirect('/home/index');
        exit;
    }

    /**
     * Check if user is logged in
     */
    public static function isLogged()
    {
        if(!isset($_SESSION['logged']) || 1 !== $_SESSION['logged'])
        {
            return false;
        }
        return true;
    }

    /**
     * Logout user
     */
    public function logout()
    {
        $_SESSION = array();
        setcookie("email", "", time() - 3600);
        Utilities::redirect('/home/index');
    }

}
