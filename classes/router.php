<?php

require_once('utilities.php');

/**
 * Router class
 */
class Router
{
    protected $executables;

    function __construct()
    {
        $this->executables = array(
            'search' => array('submit'),
            'user' => array('postRegister', 'postLogin', 'isLogged'),
            'connection' => array('connect', '_errorAction', 'query')
        );
    }

    /**
     * Check if route is valid and return class/method
     *
     * @param void
     *
     * @return array containing class and method to be executed
     */
    public function runRouter()
    {
        // Redirect from index.php to home page
        if(strpos($_SERVER['REQUEST_URI'], 'index.php'))
        {
            Utilities::redirect('/home/index');
        }

        $ret = array_filter(explode('/', $_SERVER['REQUEST_URI']));

        if(sizeof($ret) !== 2)
        {
            return false;
        }

        // Check if user is trying to access forbidden methods
        if(array_key_exists($ret[1], $this->executables))
        {
            if(in_array($ret[2], $this->executables[$ret[1]]) && 'GET' === $_SERVER['REQUEST_METHOD'])
            {
                return false;
            }
        }
        return array_combine(array('class', 'method'), $ret);
    }

}
