<?php 

require_once('view.php');

/**
 * Home class
 */
class Home extends View
{

    /**
     * Show home page
     */
    public function index()
    {
        echo $this->addData(array('pageTitle' => 'Zadatak', 'templateName' => 'partials/homeScreen'))->fetch('layout.master.tpl');
        return true;
    }
}
