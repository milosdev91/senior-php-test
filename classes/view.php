<?php

/**
 * View class
 */
class View {

    protected $data;

	public function __construct()
    {
        $this->data = NULL;
    }

    /**
     * Prepare and include view
     *
     * @param string $tplName - view name
     *
     * @return string $content - view content
     */
    public function fetch($tplName='layout.master.tpl')
    {
        $contents = '';
        $htmlData = isset($this->data) ? $this->data : array();
        foreach($htmlData as $k => $v)
        {
           $$k = $v;
        }
        ob_start();
        include(VIEWS_DIR . $tplName);
        $contents = ob_get_contents();
        ob_end_clean();

        return $contents;
    }

    /**
     * Add data to view
     *
     * @param mixed $data - data for view
     * @param string $value - view name
     *
     */
    public function addData($data, $value = '')
    {
        if(is_array($data))
        {
            $this->data = empty($this->data) ? $data : array_merge($this->data, $data);
        }
        else
        {
            if(!empty($data))
            {
                $this->data[$data] = $value;
            }
        }
        return $this;
    }
}
?>
