<?php

require_once('utilities.php');
require_once('connection.php');
require_once('view.php');

/**
 * 
 */
class Search
{
    private $link;
    private $view;
    private $utilities;
    private $connection;
    
    function __construct()
    {
        $this->view = new View;
        $this->connection = new Connection;
        $this->utilities = new Utilities;
        $this->link = $this->connection->connect();
    }

    /**
     * Show results page
     *
     * @param array $data - results
     *
     * @return true
     */
    private function index($data=array())
    {
        echo $this->view->addData(array('templateName' => 'partials/searchPage', 'data' => $data))->fetch('layout.master.tpl');
        return true;
    }

    /**
     * Search by keyword
     *
     * @param void
     *
     * @return array - query results
     */
    public function submit()
    {
        if(!User::isLogged())
        {
            $_SESSION['message'] = "Please login";
            echo $this->view->addData(array('templateName' => 'partials/loginForm'))->fetch('layout.master.tpl');
            return true;
        }

        try
        {
            $search = mysqli_real_escape_string($this->link, trim($_POST['search']));

            if(!isset($search) || !$search)
            {
                $_SESSION['message'] = "Provide search parameter.";
                echo $this->view->addData(array('pageTitle' => 'Zadatak Search','templateName' => 'partials/error'))->fetch('layout.master.tpl');
                return true;
            }

            $query = "SELECT email FROM users WHERE email LIKE '%$search%' OR name LIKE '%$search%'";
            $row = $this->link->query($query);
        }
        catch(Exception $e)
        {
            // use for debugging
        }
        $return = array();
        while ($result = mysqli_fetch_assoc($row))
        {
            $return[] = $result["email"];
        }

        return $this->index($return);
    }
}
