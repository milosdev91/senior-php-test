<?php

if(!defined('ROOT_DIR'))
{
	define('ROOT_DIR' , dirname(dirname(__FILE__)) . '/');
}

define('CLASSES_DIR' , ROOT_DIR . 'classes/');
define('VIEWS_DIR' , ROOT_DIR . 'views/');

// DB Connection params
DEFINE('DB_HOST', '127.0.0.1');
DEFINE('DB_USER', 'root');
DEFINE('DB_PASSWORD', '');
DEFINE('DB_NAME', 'zadatak');

// Dev's email (error handling)
DEFINE('DEV_EMAIL', 'milos91za@gmail.com');
DEFINE('DEFAULT_ERROR_MAIL_SUBJECT', 'Local error message');
